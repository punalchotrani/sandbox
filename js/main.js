let superHeroData = [
  {
    'superhero': 'Superman',
    'publisher': 'DC Comics',
    'alter_ego': 'Clark Kent',
    'first_appearance': 'Action Comics #1',
    'characters': 'Kal-El'
  },
  {
    'superhero': 'Batman',
    'publisher': 'DC Comics',
    'alter_ego': 'Bruce Wayne',
    'first_appearance': 'Detective Comics #27',
    'characters': 'Bruce Wayne'
  },
  {
    'superhero': 'Flash',
    'publisher': 'DC Comics',
    'alter_ego': 'Jay Garrick',
    'first_appearance': 'Flash Comics #1',
    'characters': 'Jay Garrick, Barry Allen, Wally West, Bart Allen'
  },
  {
    'superhero': 'Green Lantern',
    'publisher': 'DC Comics',
    'alter_ego': 'Alan Scott',
    'first_appearance': 'All-American Comics #16',
    'characters': 'Alan Scott, Hal Jordan, Guy Gardner, John Stewart, Kyle Raynor, Jade, Sinestro, Simon Baz'
  },
  {
    'superhero': 'Green Arrow',
    'publisher': 'DC Comics',
    'alter_ego': 'Oliver Queen',
    'first_appearance': 'More Fun Comics #73',
    'characters': 'Oliver Queen'
  },
  {
    'superhero': 'Wonder Woman',
    'publisher': 'DC Comics',
    'alter_ego': 'Princess Diana',
    'first_appearance': 'All Star Comics #8',
    'characters': 'Princess Diana'
  },
  {
    'superhero': 'Martian Manhunter',
    'publisher': 'DC Comics',
    'alter_ego': 'J\'onn J\'onzz',
    'first_appearance': 'Detective Comics #225',
    'characters': 'Martian Manhunter'
  },
  {
    'superhero': 'Robin/Nightwing',
    'publisher': 'DC Comics',
    'alter_ego': 'Dick Grayson',
    'first_appearance': 'Detective Comics #38',
    'characters': 'Dick Grayson'
  },
  {
    'superhero': 'Blue Beetle',
    'publisher': 'DC Comics',
    'alter_ego': 'Dan Garret',
    'first_appearance': 'Mystery Men Comics #1',
    'characters': 'Dan Garret, Ted Kord, Jaime Reyes'
  },
  {
    'superhero': 'Black Canary',
    'publisher': 'DC Comics',
    'alter_ego': 'Dinah Drake',
    'first_appearance': 'Flash Comics #86',
    'characters': 'Dinah Drake, Dinah Lance'
  },
  {
    'superhero': 'Spider Man',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Peter Parker',
    'first_appearance': 'Amazing Fantasy #15',
    'characters': 'Peter Parker'
  },
  {
    'superhero': 'Captain America',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Steve Rogers',
    'first_appearance': 'Captain America Comics #1',
    'characters': 'Steve Rogers'
  },
  {
    'superhero': 'Iron Man',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Tony Stark',
    'first_appearance': 'Tales of Suspense #39',
    'characters': 'Tony Stark'
  },
  {
    'superhero': 'Thor',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Thor Odinson',
    'first_appearance': 'Journey into Myster #83',
    'characters': 'Thor Odinson'
  },
  {
    'superhero': 'Hulk',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Bruce Banner',
    'first_appearance': 'The Incredible Hulk #1',
    'characters': 'Bruce Banner'
  },
  {
    'superhero': 'Wolverine',
    'publisher': 'Marvel Comics',
    'alter_ego': 'James Howlett',
    'first_appearance': 'The Incredible Hulk #180',
    'characters': 'James Howlett'
  },
  {
    'superhero': 'Daredevil',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Matthew Michael Murdock',
    'first_appearance': 'Daredevil #1',
    'characters': 'Matthew Michael Murdock'
  },
  {
    'superhero': 'Hawkeye',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Clinton Francis Barton',
    'first_appearance': 'Tales of Suspense #57',
    'characters': 'Clinton Francis Barton'
  },
  {
    'superhero': 'Cyclops',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Scott Summers',
    'first_appearance': 'X-Men #1',
    'characters': 'Scott Summers'
  },
  {
    'superhero': 'Silver Surfer',
    'publisher': 'Marvel Comics',
    'alter_ego': 'Norrin Radd',
    'first_appearance': 'The Fantastic Four #48',
    'characters': 'Norrin Radd'
  }
]

class addSuperHeroData {
  constructor (superhero, publisher, alter_ego, first_appearance, characters) {
    this.superhero = superhero
    this.publisher = publisher
    this.alter_ego = alter_ego
    this.first_appearance = first_appearance
    this.characters = characters
  }
}

//let deadpoolObj = new addSuperHeroData('Deadpool', 'Marvel Comics', 'Wade Wilson', 'New Mutants #98', 'Wade Wilson');
superHeroData.push(new addSuperHeroData('Deadpool', 'Marvel Comics', 'Wade Wilson', 'New Mutants #98', 'Wade Wilson'))
superHeroData.push(new addSuperHeroData('Black Panther', 'Marvel Comics', 'T\'Challa', 'Black Panther #1', ''))

const superheroDetails = (superhero_name, details) => {
  let value
  for (let key in superHeroData) {
    if (superHeroData[key].superhero === superhero_name) {
      value = superHeroData[key][details]
      break
    } else if (typeof superhero_name === 'undefined') {
      value = 'Please enter a superhero name'
    } else {
      value = 'Your superhero does not exist in our database'
    }
  }
  return value
}

let animalCounter = (animal) => {
  let count = 0
  return () => { // This is my closure
    count += 1
    return (count === 1) ? count + ' ' + animal : count + ' ' + animal + 's'
  }
}

let spiderman = superheroDetails('Spider Man', 'alter_ego')
//let deadpool = superheroDetails('Deadpool','alter_ego' );

let add = (x, y) => x + y

/*
let bye = () => {
  console.log('Goodbye');
};
*/

/*function executeGreeting (func) {
  func()
}

executeGreeting(() => setTimeout(console.log('Goodbye')), 2000);*/
function greeting(arg){
  console.log(arg);
}

//setInterval(() => { greeting('Hellooooo') }, 100);

//setInterval();



const loggerHero = {
  Superhero : superheroDetails('Deadpool','alter_ego' ),
  FirstAppearance : superheroDetails('Deadpool','publisher' )
};

function logPublisher(hero){
  console.log(`${hero.Superhero} first appeared in the ${hero.FirstAppearance}`);
}
setTimeout(() => {
  logPublisher(loggerHero);
}, 2000);

/**  Callback vs Promises  **/

/**  Callback example  **/

function first(value, callbackFunc) {
  callbackFunc( value * 2, false );
}

function second(value, callbackFunc) {
  callbackFunc( value * 2, false );
}

function third(value, callbackFunc) {
  callbackFunc( value * 2, false );
}

first(2, function(firstvalue, err){
  if(!err) {
    second(firstvalue, function(secondvalue, err){
      if(!err) {
        third(secondvalue, function(thirdvalue, err){
          if(!err) {
            console.log(thirdvalue);
          }
        });
      }
    });
  }
});

/**  Promise example  **/
function pFirst(value) {
  return value * 2;
}

function pSecond(value) {
  return value * 2 ;
}

function pThird(value) {
  return value * 2 ;
}



const promiseNew = new Promise(function(resolve, reject){
  resolve(2);
});
promiseNew.then(pFirst).then(pSecond).then(pThird).then(function (response) {
  console.log(response);
})


if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports.add = add
  module.exports.superheroDetails = superheroDetails
}
