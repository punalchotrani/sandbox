<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>
    <meta name="description" content="">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link type="text/plain" rel="author" href="/humans.txt" />

    <!-- 
    ###############################
    S T Y L E S
    ###############################
     -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="dist/css/styles.css">

</head>
<body class="sparky bootstrap home no-background">
<header>
    <div class="navbar navbar-default navbar-static-top no-margin">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <i class="fa fa-code fa-lg" aria-hidden="true"></i>
                    <strong>Punal Chotrani</strong>
                </a>
            </div>
        </div>
    </div>
</header>
<div class="container-fluid">
    <div class="row nature-background">
        <div class="col-md-12">
            <div class="centered text-center padding full-height">
                <h1>New site coming soon</h1>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->

<footer class="footer">
    <div class="container">
        <p class="text-muted">Copyright: Punal Chotrani</p>
    </div>
</footer>





<!--
###############################
S C R I P T S
###############################
 -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="dist/js/jquery.min.js"><\/script>')
</script>
<!-- Your custom scripts compressed  -->
<!--<script src="dist/js/scripts.min.js" ></script>-->
<script src="dist/js/scripts.min.js"></script>

<!-- Add scripts that are not critical but simply enhance - like social sharing widgets -->
<script type="text/javascript">
    function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "dist/js/defer.min.js";
        document.body.appendChild(element);
    }
    if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
</script>

<section class="container">
    <div class="row">
        <div id="message" class="text-center lead"></div>
    </div>
</section>

</body>

</html>
