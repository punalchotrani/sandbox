let expect = require('chai').expect;

describe('Mocha', function(){
  //Test spec unit tests
  it('should run tests with mocha', function(){
    expect(true).to.be.ok;
  });
});

describe('Addition', function(){
  let add = require('../js/main').add;
  it('should add', function(){
    expect(add(2,2)).to.be.equal(4);
    expect(add(10,5)).to.be.equal(15);
  });
});

describe('SuperHero', function(){
  let superheroDetails = require('../js/main').superheroDetails;
  it('Should get the superhero alter ego', () => {
    expect(superheroDetails('Spider Man','alter_ego' )).to.be.equal('Peter Parker');
  });
});
